package com.company;
import java.util.ArrayList;
import java.lang.String;

public class KnowledgeBase {
    ArrayList<Edge> edges = new ArrayList<Edge>();

    void saveStatement(String statement){
        String[] division = statement.split(" ");
        Concept c1 = new Concept();
        c1.conceptName = division[0];
        Concept c2 = new Concept();
        c2.conceptName = division[2];
        Edge e = new Edge();
        e.polarity = division[1].equals("IS-A");
        e.subConcept = c1;
        e.superConcept = c2;
        edges.add(e);
    }

    void findPaths(String query,ArrayList<ArrayList<Edge>> paths){
        String[] division = query.split(" ");
        for (Edge edge : edges) {      // adding all edges in kb that have same subconcept as query to list of paths
            if (edge.subConcept.conceptName.equals(division[0])) {
                ArrayList<Edge> tempPath = new ArrayList<Edge>();
                tempPath.add(edge);
                paths.add(tempPath);
            }
        }
        ArrayList<Edge> directEdge = new ArrayList<Edge>();
        for(int i = 0;i<paths.size();i++){
            for(int j = 0;j<paths.get(i).size();j++){
                if(paths.get(i).get(j).superConcept.conceptName.equals(division[2])){
                    directEdge.add(paths.get(i).get(j));
                    paths.remove(i);
                    break;
                }
            }
        }
        for(int i = 0;i<paths.size();i++){
            do {
                if ((!paths.get(i).get(paths.get(i).size() - 1).polarity) || (paths.get(i).get(paths.get(i).size() - 1).superConcept == null)) {
                    if (!paths.get(i).get(paths.get(i).size() - 1).superConcept.conceptName.equals(division[2])) {
                        paths.remove(i);
                        break;
                    }
                }
                else {
                    ArrayList<Edge> outgoingEdges = new ArrayList<Edge>();
                    ArrayList<Edge> prevPath = new ArrayList<Edge>();
                    boolean found = false;
                    for (Edge edge : edges) {
                        if (!paths.get(i).get(paths.get(i).size() - 1).superConcept.conceptName.equals(division[2])) {
                            if (paths.get(i).get(paths.get(i).size() - 1).superConcept.conceptName.equals(edge.subConcept.conceptName)) {
                                outgoingEdges.add(edge);
                                found = true;
                            }
                        }
                    }
                    if(!found){
                        paths.remove(i);
                        break;
                    }
                    for (int j = 0; j < outgoingEdges.size(); j++) {
                        if (outgoingEdges.size() == 1) {
                            paths.get(i).add(outgoingEdges.get(j));
                        }
                        else {
                            if (j == 0) {
                                copyPath(prevPath, paths.get(i));
                            }
                            else {
                                ArrayList<Edge> newPath = new ArrayList<Edge>();
                                copyPath(newPath, prevPath);
                                newPath.add(outgoingEdges.get(j));
                                paths.add(newPath);
                            }
                        }
                    }
                    if (outgoingEdges.size() != 1)
                        paths.get(i).add(outgoingEdges.get(0));
                }
            }while(!paths.get(i).get(paths.get(i).size() - 1).superConcept.conceptName.equals(division[2]));
        }
        if(directEdge.size()>0)
            paths.add(directEdge);
    }

    void displayPaths(String query, ArrayList<ArrayList<Edge>> paths){
        String[] division = query.split(" ");
        for (ArrayList<Edge> path : paths) {
            System.out.print(division[0]);
            for (Edge edge : path) {
                if (edge.polarity) {
                    System.out.print(" IS-A " + edge.superConcept.conceptName);
                } else {
                    System.out.print(" IS-NOT-A " + edge.superConcept.conceptName);
                }
            }
            System.out.print("\n");
        }

    }

    void copyPath(ArrayList<Edge> newPath,ArrayList<Edge> prevPath){
        for (Edge edge : prevPath) {
            Edge e = new Edge();
            e.polarity = edge.polarity;
            Concept c1 = new Concept();
            Concept c2 = new Concept();
            c1.conceptName = edge.subConcept.conceptName;
            c2.conceptName = edge.superConcept.conceptName;
            e.subConcept = c1;
            e.superConcept = c2;
            newPath.add(e);
        }
    }

    void shortestPath(String query, ArrayList<ArrayList<Edge>> paths){
        System.out.println("Preferred Path (Shortest Distance):");
        int sDist = Integer.MAX_VALUE;
        for (ArrayList<Edge> edgeArrayList : paths) {
            if (edgeArrayList.size() < sDist) {
                sDist = edgeArrayList.size();
            }
        }
        String[] division = query.split(" ");
        for (ArrayList<Edge> path : paths) {
            if (path.size() == sDist) {
                System.out.print(division[0]);
                for (Edge edge : path) {
                    if (edge.polarity) {
                        System.out.print(" IS-A " + edge.superConcept.conceptName);
                    } else {
                        System.out.print(" IS-NOT-A " + edge.superConcept.conceptName);
                    }
                }
                System.out.print("\n");
            }
        }
    }

    void inferentialPath(String query,ArrayList<ArrayList<Edge>> paths){
        ArrayList<ArrayList<Edge>> infPaths =  new ArrayList<ArrayList<Edge>>();  //used to store all inferential paths(answer)
        System.out.println("Preferred Path (Inferential Distance):");
        for (ArrayList<Edge> path : paths) {      //Check for redundant edges
            boolean foundRed = false;
            for (Edge value : path) {
                String edge;
                if (value.polarity)
                    edge = value.subConcept.conceptName + " IS-A " + value.superConcept.conceptName;
                else
                    edge = value.subConcept.conceptName + " IS-NOT-A " + value.superConcept.conceptName;
                boolean edgePolarity = true;
                if (edge.contains("IS-NOT-A"))
                    edgePolarity = false;
                ArrayList<ArrayList<Edge>> redTest = new ArrayList<ArrayList<Edge>>(); //used for redundancy tests
                findPaths(edge, redTest);
                for (int k = 0; k < redTest.size(); k++) {
                    if (redTest.get(k).get(redTest.get(k).size() - 1).polarity != edgePolarity) {
                        redTest.remove(k);
                        k--;
                    }
                }
                if (redTest.size() > 1) {
                    foundRed = true;
                    break;
                }
            }
            if (!foundRed)
                infPaths.add(path);
        }
        for(int i = 0;i<infPaths.size();i++){       //Check for pre-emption in paths with no redundant edges
            Edge lastEdge = new Edge();
            lastEdge = infPaths.get(i).get(infPaths.get(i).size()-1);
            ArrayList<Edge> shortcuts = new ArrayList<Edge>();  //ArrayList that holds all edges that have a shortcut to superConcept of last edge
            for(int j = infPaths.get(i).size()-2;j>=0;j--){
                for (Edge edge : edges) {
                    if ((edge.subConcept.conceptName.equals(infPaths.get(i).get(j).subConcept.conceptName)) && (edge.superConcept.conceptName.equals(lastEdge.superConcept.conceptName)))
                        shortcuts.add(edge);
                }
            }
            for (Edge shortcut : shortcuts) {
                if (shortcut.polarity != lastEdge.polarity) {
                    infPaths.remove(i);
                    break;
                }
            }
        }
        displayPaths(query,infPaths);
    }
}

