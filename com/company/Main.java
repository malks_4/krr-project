package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.lang.String;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File kb2File = new File("C:\\Users\\malco\\IdeaProjects\\KRRproject\\KRR2\\src\\KB2.txt");
        Scanner scnr = new Scanner(kb2File);
        Scanner keyboard = new Scanner(System.in);
        KnowledgeBase kb = new KnowledgeBase();
        String statement;
        while(scnr.hasNextLine()) {
            statement = scnr.nextLine();
            System.out.println(statement);
            kb.saveStatement(statement);
        }
        System.out.println("Please enter your query:");
        String query = keyboard.nextLine();
        ArrayList<ArrayList<Edge>> paths = new ArrayList<ArrayList<Edge>>();
        kb.findPaths(query,paths);
        System.out.println("Paths found:");
        kb.displayPaths(query,paths);
        System.out.print("\n");
        kb.shortestPath(query,paths);
        System.out.print("\n");
        kb.inferentialPath(query,paths);
    }
}
